# Equity Analysis Tools

## Installation

```
$> mkvirtualenv -p python3.6 btm
$> workon btm
(btm) $> pip install -r requirements.txt
```

need yfinance version that fixes a bug: 
```
pip install git+https://github.com/rodrigobercini/yfinance.git
```

## Getting data

```
(btm) $> python scripts/scraping/download_liquid_options.py 
```

Downloads list of 500 most liquid stocks and ETFS to `.csv` files in `data/options` with the current
date appended.
