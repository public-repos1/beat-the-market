import numpy as np
import warnings
# from btm.securities import process_security

buy_sell_actions = ['Buy to Close', 'Sell to Open', 'Buy', 'Sell', 
        'Reinvest Shares', 'Buy to Open', 'Exchange or Exercise', 'Assigned', 
        'Sell to Close'
]
# n.b. some journal actions are not transfers, check 'amount' field
transfer_actions = ['MoneyLink Deposit','Journal','Wire Funds Received',
        'MoneyLink Transfer', 'Security Transfer','Funds Received'
]

div_int_actions = ['Bank Interest','Cash Dividend','Qualified Dividend', 
    'Pr Yr Cash Div','Margin Interest', 'Qual Div Reinvest',
    'Long Term Cap Gain Reinvest', 'Service Fee','ADR Mgmt Fee', 
    'Foreign Tax Paid','Pr Yr Div Reinvest'
]
# other possible actions:	
# ['Reinvest Dividend', 'Name Change', 'Stock Merger']



def process_transaction(df_row):
    ''' Classify a row of a pandas DataFrame into one of the transaction
    types, and create an object with its information.
    '''
    date = df_row['Date']
    amount = df_row['Amount']
    action = df_row['Action'] 
    quantity = df_row['Quantity']

    transaction = None

    if action in buy_sell_actions:
        # security = process_security(df_row)
        transaction = Strategy(amount, date, [])

    elif action in transfer_actions: 
        transaction = Transfer(amount, date)
    
    elif action in div_int_actions:
        transaction = DividendOrInterest(amount, date)
    else:
        warnings.warn('Action {} not recognized when processing transaction, ignoring'
            .format(action))

    return transaction


        


def group_transactions(transaction_list):
    ''' Given a list of atomic transactions, group them into strategies
    based on dates and ticker symbols. (e.g. group buying and selling puts
    at different strikes for the same expiration on the same date into a 
    spread.
    '''
    return transaction_list


class Transaction:

    def __init__(self, amount, date):
        self.amount = amount
        # self.action = action
        self.date = date


class Strategy(Transaction):

    ''' Transacion representing a buy or sell of a stock, ETF, or group of
    options and/or stocks.
    '''

    def __init__(self, amount, date, securities):

        super().__init__(amount, date)
        self.securities = securities
        self.value = 0.

        self.calculate_value()


    def calculate_value(self):
        ''' Get current value based on underlying securities.
        '''
        warnings.warn('Strategy.calculate_value not implemented yet.')



class Transfer(Transaction):
    
    ''' Transaction representing a deposit or withdrawl from another 
    account.
    '''

    def __init__(self, amount, date):
        
        super().__init__(amount, date)
        self.amount = amount
        self.date = date


class DividendOrInterest(Transaction):
    
    ''' Transaction representing a deposit such as a dividend
    or bank interest.
    '''

    def __init__(self, amount, date):
        
        super().__init__(amount, date)
        self.amount = amount
        self.date = date


