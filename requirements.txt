beautifulsoup4==4.9.1
matplotlib==3.2.1
numpy==1.18.4
pandas==1.0.3
pandas_datareader==0.8.1
selenium==3.141.0
yfinance==0.1.54
