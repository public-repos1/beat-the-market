import sys
sys.path.append('.')

import numpy as np
import pandas as pd
from btm.portfolio import Portfolio, buy_sell_actions, transfer_actions, div_int_actions
from utils import preprocess_dates, preprocess_dollar_values, \
    get_latest_transaction_file, parse_option_symbol

#%% get latest csv from data directory
latest_file = get_latest_transaction_file()
df_raw = pd.read_csv(latest_file, header=1)
pf = Portfolio.from_csv(latest_file)
pf.set_initial_balance(874.)

#%% 
import datetime
d1 = datetime.date.fromisoformat('2019-05-23')
d2 = datetime.date.fromisoformat('2020-05-23')

#%% determine holdings
balance = pf.calculate_balance()
print(balance)
