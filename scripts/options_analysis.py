import os
import glob

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import pandas as pd
import numpy as np
import datetime
import sys
sys.path.append('.')
from utils import preprocess_dates, get_latest_transaction_file

#%% get latest csv from data directory
latest_file = get_latest_transaction_file()
df = pd.read_csv(latest_file, header=1)
preprocess_dates(df)

#%%
df['Price'] = df['Price'].replace('[\$,]', '', regex=True).astype(float)
df['Amount'] = df['Amount'].replace('[\$,]', '', regex=True).astype(float)

#%%
option_actions = ['Sell to Close', 'Buy to Open', 'Sell to Open', 'Buy to Close']
option_trades = df.loc[df['Action'].isin(option_actions)]

#%%
amounts = option_trades['Amount']
total_gain = amounts.sum()
total_spent = -amounts[amounts<0].sum()

#%%
print(total_gain)
print(100*total_gain/total_spent)


#%% plot gain/loss on a per-day basis instead
unique_date_strs = option_trades.Date.unique()
unique_dates = [datetime.datetime.strptime(d, '%m/%d/%Y') for d in unique_date_strs]
daily_gains = []

for date in unique_date_strs:
    day_gains = option_trades[option_trades.Date == date].Amount.sum()
    daily_gains.append(day_gains)

#%%
cumulative_daily_gains = np.cumsum(daily_gains)

#%%
years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
years_fmt = mdates.DateFormatter('%Y')

# fig, ax = plt.subplots(figsize=(12,8))
fig = plt.figure(figsize=(15,8))

pos_gains = [max(d, 0) for d in daily_gains]
neg_gains = [min(d, 0) for d in daily_gains]
plt.bar(unique_dates, pos_gains, width=2.0, color='yellowgreen')
plt.bar(unique_dates, neg_gains, width=2.0, color='tomato')
plt.plot(unique_dates, cumulative_daily_gains)
plt.show()

#%% search for the trades with the highest losses
