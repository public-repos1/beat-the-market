import os
import glob

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import numpy as np
import pandas as pd
import datetime
import sys
sys.path.append('.')
from utils import preprocess_dates, get_latest_transaction_file

#%% get latest csv from data directory
latest_file = get_latest_transaction_file()
df = pd.read_csv(latest_file, header=1)
preprocess_dates(df)

#%%
df['Price'] = df['Price'].replace('[\$,]', '', regex=True).astype(float)
df['Amount'] = df['Amount'].replace('[\$,]', '', regex=True).astype(float)

#%%
option_actions = ['Sell to Close', 'Buy to Open', 'Sell to Open', 'Buy to Close']
option_trades = df.loc[df['Action'].isin(option_actions)]

#%% TODO: find options that were exercised, track the subsequent purchase and sale of the underlying
exercises = df.loc[df['Action'] == 'Exchange or Exercise']
assignments = df.loc[df['Action'] == 'Assigned']

exercise_symbols = exercises['Symbol'].to_list()
assignment_symbols = assignments['Symbol'].to_list()

print(exercises)
print(exercise_symbols)

print(assignments)
print(assignment_symbols)
print('\n')


#%%
def parse_option_symbol(option_symbol):
    symbol, date, strike_price, strategy = option_symbol.split(' ')
    date = datetime.datetime.strptime(date, '%m/%d/%Y')
    strike_price = float(strike_price)

    return symbol, date, strike_price, strategy



#%% TODO: loop over all exercises
def get_exercise_amount(exercise, all_transactions_df):

    ex_sym = exercise['Symbol'] # .values[0]
    # TODO: have to look for transactions that happened on the same date
    # TODO: also look for assignment actions
    option_purchase = all_transactions_df.loc[all_transactions_df['Symbol'] == ex_sym].loc[all_transactions_df['Action'] != 'Exchange or Exercise']
    amount_paid = option_purchase['Amount'].to_list()[0]

    symbol, date, strike, strategy = parse_option_symbol(ex_sym)

    num_contracts = option_purchase['Quantity'].to_list()[0]
    buy_total = strike * num_contracts * 100

    # confirm buy total with subsequent trade
    exercise_purchase = df.iloc[0:(option_purchase.index[0])].loc[df['Symbol'] == symbol].loc[df['Price'] == strike]

    if (exercise_purchase['Amount'].to_list()[-1]) != -buy_total:
        print('Exercise amount does not match buy total: ')
        print('\t', exercise)
        print('\t', exercise_purchase['Amount'])
        print(buy_total)



    return -buy_total

symbol, date, strike_price, strategy = parse_option_symbol(exercise_symbols[0])
quantity = exercises.iloc[0]['Quantity']
ex_date = exercises.iloc[0]['Date']
print(symbol)
print(date)
print(strike_price)
print(strategy)
print(quantity)
print(ex_date)



'''
#%%
for k in range(len(exercises)):
    get_exercise_amount(exercises.iloc[k], df)

#%%
exercises
ex = exercises.iloc[0]
ex
#%%
df.iloc[165:171]
#%% TODO: find subsequent sell (or position) and subtract
'''
