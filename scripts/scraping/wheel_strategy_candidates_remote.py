import os
import glob
import json
import datetime

import numpy as np
import pandas as pd
import yfinance as yf


def get_latest_liquid_options(data_dir='data/options'):
    etf_option_files = glob.glob(os.path.join(data_dir, 'most_liquid_etfs_*.csv'))
    etf_option_files.sort()
    latest_file = etf_option_files[-1]
    return latest_file


def download_option_info(symbol, symbol_dir):
    os.makedirs(symbol_dir)
    ticker = yf.Ticker(symbol)
    print('Downloading ticker info for {}'.format(symbol))
    ticker_info = ticker.info
    with open(os.path.join(symbol_dir, 'ticker_info.json'), 'w') as f:
        json.dump(ticker_info, f)

    option_dates = [datetime.datetime.fromisoformat(d).date() \
                    for d in ticker.options]

    print('Downloading option info for {}'.format(symbol))
    today_date = datetime.datetime.today().date()
    near_dates = [d for d in option_dates if (d - today_date).days >= 30 and \
                                             (d - today_date).days <= 45]

    print(near_dates)
    for near_date in near_dates:
        print('Downloading option info for date {}'.format(near_date.isoformat()))
        near_date_str = near_date.isoformat()
        opt_chain = ticker.option_chain(near_date_str)
        calls = opt_chain[0]
        puts = opt_chain[1]
        date_dir = os.path.join(symbol_dir, near_date_str)
        os.makedirs(date_dir, exist_ok=True)

        calls.to_csv(os.path.join(date_dir, 'calls.csv'))
        puts.to_csv(os.path.join(date_dir, 'puts.csv'))

def collect_options_df(symbol_dir, options_file):
    # iterate through dates and collect strike prices
    # TODO: optimize
    date_subdirs = [d for d in os.listdir(symbol_dir) \
                    if os.path.isdir(os.path.join(symbol_dir, d))]

    df_opts = pd.DataFrame(columns=['ticker', 'price', 'date', 'type', 'strike',
                                    'open_interest', 'last', 'ask', 'bid'])
    for subdir in date_subdirs:
        date_dir = os.path.join(symbol_dir, subdir)
        exp_date = datetime.date.fromisoformat(subdir)
        
        # compile calls
        calls_file = os.path.join(date_dir, 'calls.csv')
        calls = pd.read_csv(calls_file)
        
        for idx,row in calls.iterrows():
            new_row = {'ticker': symbol, 
                        'price': ticker_info['regularMarketPrice'],
                        'date': exp_date,
                        'type': 'C',
                        'strike': row.strike,
                        'open_interest': row.openInterest,
                        'last': row.lastPrice,
                        'ask': row.ask,
                        'bid': row.bid
                        }
            df_opts = df_opts.append(new_row, ignore_index=True)
       
        # compile puts
        puts_file = os.path.join(date_dir, 'puts.csv')
        puts = pd.read_csv(puts_file)
        
        for idx,row in puts.iterrows():
            new_row = {'ticker': symbol, 
                        'price': ticker_info['regularMarketPrice'],
                        'date': exp_date,
                        'type': 'P',
                        'strike': row.strike,
                        'open_interest': row.openInterest,
                        'last': row.lastPrice,
                        'ask': row.ask,
                        'bid': row.bid
                        }
            df_opts = df_opts.append(new_row, ignore_index=True)
    
    df_opts.to_csv(options_file)


etf_option_file = get_latest_liquid_options()
df_syms = pd.read_csv(etf_option_file)

all_df_opts = pd.DataFrame(columns=['ticker', 'price', 'date', 'type', 'strike',
                                'open_interest', 'last', 'ask', 'bid'])

# for symbol in df_syms.symbol:
# retrieve current option chain
# symbol = df_syms.symbol.iloc[0]
for symbol in df_syms.symbol:
    symbol_dir = os.path.join('data', 'tmp', symbol)


    # try to download symbol info if DNE
    if not os.path.exists(symbol_dir):
        download_option_info(symbol, symbol_dir)


    # get market price
    with open(os.path.join(symbol_dir, 'ticker_info.json'), 'r') as f:
        ticker_info = json.load(f)

    options_file = os.path.join(symbol_dir, 'current_options.csv')
    if not os.path.exists(options_file):
        collect_options_df(symbol_dir, options_file)
    df_opts = pd.read_csv(options_file)
    if len(df_opts) == 0:
        continue
    df_opts.drop(columns=['Unnamed: 0'], inplace=True)

    # if loading from csv, dates are loaded as strings
    if type(df_opts.date[0]) is str:
        f = lambda d: datetime.datetime.fromisoformat(d).date()
        df_opts.date = df_opts.date.map(f)

    # create DTE column
    today_date = datetime.datetime.today().date()
    dte = (df_opts.date - today_date)
    df_opts['dte'] = dte.map(lambda d: d.days)

    # process calls
    calls = df_opts[df_opts.type == 'C']
    itm_calls = calls[calls.price < calls.strike]

    mid = (itm_calls.ask+itm_calls.bid)/2
    assigned_profit = mid + (itm_calls.strike - itm_calls.price)
    unassgnd_profit = mid

    df_opts.loc[list(itm_calls.index), 'assigned_profit'] = assigned_profit
    df_opts.loc[list(itm_calls.index), 'unassgnd_profit'] = unassgnd_profit

    # process puts
    puts = df_opts[df_opts.type == 'P']
    itm_puts = puts[puts.price > puts.strike]

    mid = (itm_puts.ask+itm_puts.bid)/2
    profit = mid

    df_opts.loc[list(itm_puts.index), 'assigned_profit'] = profit
    df_opts.loc[list(itm_puts.index), 'unassgnd_profit'] = profit
    df_opts.dropna(subset=['assigned_profit'], inplace=True)
    df_opts.dropna(subset=['unassgnd_profit'], inplace=True)
    df_opts.reset_index(inplace=True)

    # calculate roi, factoring in a weekend for good measure
    a_roi = (df_opts.assigned_profit / df_opts.price) * (365 / (df_opts.dte + 2)) 
    u_roi = (df_opts.unassgnd_profit / df_opts.price) * (365 / (df_opts.dte + 2)) 

    df_opts.loc[:, 'assigned_ROI'] = a_roi
    df_opts.loc[:, 'unassgnd_ROI'] = u_roi

    avg_roi = (a_roi + u_roi) / 2
    df_opts.loc[:, 'avg_ROI'] = avg_roi
    all_df_opts = pd.concat([all_df_opts, df_opts])
