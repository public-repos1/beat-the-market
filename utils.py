import os
import glob
import re
import datetime

import numpy as np
symbol_changes = {'WTW': 'WW'}


def preprocess_dates(transactions_df):
    date_col = transactions_df['Date']
    # some dates have a weird string format, parse the later date for those
    f = lambda x: x.split(' as of ')[0]
    transactions_df['Date'] = date_col.map(f)

    # sometimes the transaction total is listed in the dates column
    idx = transactions_df.index[transactions_df['Date']=='Transactions Total']
    transactions_df.drop(idx, inplace=True)
    
    # convert into datetime objects
    date_col = transactions_df['Date']
    f = lambda d: datetime.datetime.strptime(d, '%m/%d/%Y').date()
    transactions_df['Date'] = date_col.map(f)


def preprocess_dollar_values(transactions_df):
    f = lambda x: x if x is np.nan else float(x.replace('$', ''))
    transactions_df['Price'] = transactions_df['Price'].map(f)
    transactions_df['Amount'] = transactions_df['Amount'].map(f)


def fix_symbol_changes(transactions_df, change_dict=symbol_changes):
    ''' Fix known symbol changes. '''
    transactions_df.replace({'Symbol': change_dict}, inplace=True)


def negate_sell_quantities(df):
    ''' For all actions, the "Quantity" column is positive. Negate for sell 
    actions so that the number of holdings for a security can be determind 
    with a column sum.
    '''
    df.loc[df.Action == 'Sell', 'Quantity'] = -df[df.Action == 'Sell'].Quantity
    df.loc[df.Action == 'Sell to Open', 'Quantity'] = -df[df.Action == 'Sell to Open'].Quantity
    df.loc[df.Action == 'Sell to Close', 'Quantity'] = -df[df.Action == 'Sell to Close'].Quantity


def get_latest_transaction_file(data_dir='data/'):
    transactions_files = glob.glob(os.path.join(data_dir, '*.CSV'))
    transactions_files.sort()
    latest_file = transactions_files[-1]
    return latest_file


def is_option_symbol(symbol):
    reg = "[A-Z]+ [0-9]{2}/[0-9]{2}/[0-9]{4} [0-9]+.[0-9]{2} [CP]"
    matched = re.match(reg, symbol)
    return bool(matched)


def parse_option_symbol(option_symbol):
    symbol, date, strike_price, strategy = option_symbol.split(' ')
    date = datetime.datetime.strptime(date, '%m/%d/%Y')
    strike_price = float(strike_price)

    return symbol, date, strike_price, strategy
